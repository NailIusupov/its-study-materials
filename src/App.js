import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import MainPage from './components/MainPage';
import SecondPage from './components/SecondPage';



function App() {

    return (
        <BrowserRouter>
            <header>
                <nav>
                    <Link to="/">First page</Link>
                    <Link to="/second">Second page</Link>
                </nav>
            </header>
            <Switch>
                <Route exact path="/" component={MainPage}/>
                <Route exact path="/second" component={SecondPage}/>
            </Switch>
        </BrowserRouter>
    );
}

export default App;
