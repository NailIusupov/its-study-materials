import React, {Component} from 'react';
import SearchField from './SearchField';
import ItemsList from './ItemsList';

const itemsList = [
    {
        id: 1,
        value: 'html'
    },
    {
        id: 2,
        value: 'css'
    },
    {
        id: 3,
        value: 'java script'
    },
    {
        id: 4,
        value: 'angular'
    },
    {
        id: 5,
        value: 'react'
    },
    {
        id: 6,
        value: 'vue'
    }
];

export default class MainPage extends Component{

    constructor(props) {
        super(props);

        this.state = {
            itemsListState: itemsList
        }
    }

    searchFunction = (event) => {
        const { value } = event.target;
        this.setState({ itemsListState: itemsList.filter(item => !!~item.value.indexOf(value)) })
    };

    render() {
        return (
            <section className="app-wrapper">
                <SearchField searchFunction={this.searchFunction}/>
                <ItemsList itemsListState={this.state.itemsListState}/>
            </section>
        );
    }
}