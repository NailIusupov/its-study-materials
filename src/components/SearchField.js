import React, {Component} from 'react';

export default class SearchFiled extends Component {

    render() {

        const { searchFunction } = this.props;

        return (
            <label>
                <input type="text" placeholder="search" onChange={searchFunction}/>
            </label>
        )
    }
}