import React, { Component } from 'react';

export default class ItemsList extends Component {

    render() {

        const { itemsListState } = this.props;

        return (
            <ul>
                {
                    itemsListState.map((item, index) => (
                        <li key={item.id}>{item.value}</li>
                    ))
                }
            </ul>
        )
    }
}